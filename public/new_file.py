from shutil import copyfile
import bs4
import yaml
import sys


name = ""
if len(sys.argv) <= 1:
    with open("stuff/config.yaml") as file:
        config = yaml.full_load(file)

    config["count"] += 1

    with open("stuff/config.yaml","w") as file:
        doc = yaml.dump(config,file)
    name = f'{config["count"]:03d}'
else: 
    name = sys.argv[1]

src = "stuff/prototype.html"
dst = "{}.html".format(name)

copyfile(src, dst)

# load the file
with open(dst) as inf:
    txt = inf.read()
    soup = bs4.BeautifulSoup(txt)

soup.head.title.string = name
soup.body.div.h1.string = name

# save the file again
with open(dst, "w") as outf:
    outf.write(str(soup))

index = "index.html"

with open(index) as inf:
    txt = inf.read()
    soup = bs4.BeautifulSoup(txt)

li = soup.new_tag("li")
a = soup.new_tag("a",href=f"{name}.html")
a.string = name
li.append(a)
soup.body.div.ul.append(li)

with open(index, "w") as outf:
    outf.write(str(soup))

