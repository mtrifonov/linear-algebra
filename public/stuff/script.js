
async function run() {

    var let = document.getElementsByClassName('let')
    var then = document.getElementsByClassName('then')
    var proof = document.getElementsByClassName('proof')
    build_block(let,"let","Let:");
    build_block(proof,"proof","Proof:");
    build_block(then,"then","Then:");

    var expand = document.getElementsByClassName('expand')
    build_expand(expand);
}

function build_expand(elements) {
  for (var i = 0; i< elements.length; i++) {
    var expand = elements[i];
    var preview = document.getElementById("expand_"+expand.id);
    var content = document.createElement("span")
    expand.appendChild(preview);
    MathJax.typeset();
  }
}

function build_block(elements,name,desc) {
    for (var i = 0; i < elements.length; i++) {
        var content = "<div class='h2c'>"+desc+"</div>";
        content = content + "<div class='indent'><div class='make'>";
        content = content + elements[i].innerHTML;
        content = content + "</div></div>";
        elements[i].innerHTML = content;
    }
    while (elements.length > 0) {
      elements[0].classList.remove(name);
    }
}





