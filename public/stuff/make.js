
function runMake() {
    document.onkeydown = toggleMode;
    renderClean();
}

function renderClean() {
    removeMath(document);
    elements = document.getElementsByClassName("hidden_math");
    for (var i = 0; i < elements.length; i++) {
        fix = "$".repeat(elements[i].dataset.times)
        span = document.createTextNode(fix + elements[i].dataset.math + fix);
        elements[i].parentNode.insertBefore(span,elements[i]);
    }
    MathJax.typeset();
}

function activateMakeBoxes() {
    var elements = document.getElementsByClassName('make')
    for (var i = 0; i< elements.length; i++) {
        make = elements[i]
        elements[i].classList.add("make_visible");
        make.onclick = makeClick;
        make.onmouseover = makeHover;
        make.onmouseout = makeUnhover
    }
}

function makeHover(e) {
    if (e.target.classList.contains('make')) {
        e.target.classList.add("make_active");
    }

}

function makeUnhover(e) {
    if (e.target.classList.contains('make')) {
        e.target.classList.remove("make_active");
    }
}

function deactivateMakeBoxes(loc) {
    var elements = loc.getElementsByClassName('make')
    for (var i = 0; i< elements.length; i++) {
        make = elements[i]
        make.classList.remove("make_active");
        make.onclick = function(){};
        make.onmouseover = function(){};
        make.onmouseout = function(){};
    }
}

function makeClick(e) {
    if (e.target.classList.contains('make')) {
        var edit = makeEdit(e.target);
        e.target.innerHTML = "";
        e.target.replaceWith(edit);   
        deactivateMakeBoxes(document);  
    }
}

function makeEdit(make) {
    var edit = document.createElement("textarea");
    edit.classList.add('edit');
    make = removeMath(make);
    make = reappearMath(make);
    edit.innerHTML = make.innerHTML;
    edit.onkeydown = exitEdit;
    return edit;
}

function reappearMath(make) {
    elements = make.getElementsByClassName("hidden_math");
    while (elements.length > 0) {
        var fix = "$".repeat(elements[0].dataset.times);
        elements[0].outerHTML = fix + elements[0].dataset.math + fix; 
    }

    return make;
}

function removeMath(make) {
    elements = make.getElementsByClassName("MathJax");
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
    return make;
}

executeCommand = null;

function exitEdit(e) {
    var evtobj = window.event? event : e
    if (evtobj.keyCode == 13 && evtobj.shiftKey) {
        executeCommand = null;
        el = reBox(this);
        MathJax.typeset();
        run();
        file_save();
        activateMakeBoxes();
        if(executeCommand!=null) {
            executeCommand(el);
        }
    }
}

function delete_box(src) {
    console.log(src)
    src.parentNode.removeChild(src);
    activateMakeBoxes();
}

function insert_box_above(src) {
    var box = document.createElement("div");
    box.classList.add("make");
    src.parentNode.insertBefore(box,src);
    activateMakeBoxes(); 
}

function insert_box_below(src) {
    var box = document.createElement("div");
    box.classList.add("make");
    src.after(box);
    activateMakeBoxes(); 
}

function reBox(el) {
    var make = document.createElement("div");
    make.classList.add("make");
    make.innerHTML = processHTML(el.value);
    el.replaceWith(make);
    return make;
}

var specialCommands = [
    ["OL","<ol><div class='make make_visible'>LI LI LI</div></ol>"],
    ["UL","<ul><div class='make make_visible'>LI LI LI</div></ul>"],
    ["LI", "<div class='make make_visible'><li>BOX</li></div>"],
    ["LET", "<div class='let'></div>"],  
    ["THEN", "<div class='then'></div>"],
    ["PROOF", "<div class='proof'></div>"],     
    ["CHUNK", "<div class='chunk'>BOX</div>"],   
    ["BOX","<div class='make make_visible'></div>"],   
]

var functionCommands = [
    ["EXPORT",file_export], 
    ["NEW",file_new],  
    ["LOAD",file_load],  
    ["DEL",delete_box],   
    ["ABOVE",insert_box_above],   
    ["BELOW",insert_box_below],
]

function applySpecialCommands(string) {
    for (var i = 0; i< specialCommands.length; i++) {
        command = specialCommands[i][0];
        replac = specialCommands[i][1];
        string = string.replaceAll(command,replac)
    }
    for (var i = 0; i< functionCommands.length; i++) {
        command = functionCommands[i][0];
        if (string.includes(command)) {
            string = string.replace(command,"");
            executeCommand=functionCommands[i][1];
        }

    }
    return string;

};


function processHTML(string) {
    string = applySpecialCommands(string);
    string = encodeMaths(string,true);
    return string;
}

function encodeMaths(string,append) {
    string = string.trim();
    //string = string.replace(/(\r\n|\n|\r)/gm, "");
    double_eq_pattern = /\$\$(.*?)\$\$/g
    single_eq_pattern = /\$(.*?)\$/g

    double_eq = string.split(double_eq_pattern);
    for (var i = 0; i < double_eq.length; i++) {
        if (i % 2 == 0) {
            single_eq = double_eq[i].split(single_eq_pattern);   
            for (var k = 0; k < single_eq.length; k++) {
                local_string = single_eq[k];
                single_eq[k] = k % 2 == 0 ? single_eq[k] : makeReplacement(single_eq[k],1,append)
            }
            double_eq[i] = single_eq.join("");
        } else {
            double_eq[i] = makeReplacement(double_eq[i],2,append)
        }
    }
    return double_eq.join("");
}

function makeReplacement(string,k,append) {
    fix = "$".repeat(k)
    hidden_el = document.createElement("div");
    hidden_el.classList.add("hidden_math");
    hidden_el.dataset.math = string;
    hidden_el.dataset.times = k;   
    if (append) {
        string = fix + string + fix + hidden_el.outerHTML;
    }
    else {
        string = hidden_el.outerHTML;
    }

    return string;
}

make_visibility = false;

function toggleMode() {
    var evtobj = window.event? event : e
    if (evtobj.keyCode == 49 && evtobj.shiftKey) {
        make_visibility = make_visibility ? false : true;
        if (make_visibility) {
            activateMakeBoxes();
        } else {
            hideMakeBoxes(document);
        }
    }
}

function hideMakeBoxes(loc) {
    deactivateMakeBoxes(loc);
    var elements = loc.getElementsByClassName('make')
    for (var i = 0; i< elements.length; i++) {
        make = elements[i]
        make.classList.remove("make_visible")
    }
}



