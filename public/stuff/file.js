function runFile() {
    // Check for existing file
    var existing = getCookie("martin");
    if (existing != "") {!
        file_insert(existing);
    }
}

function file_insert(file_content) {
    document.getElementById("main").innerHTML = file_content;
    renderClean();
}

function file_save() {
    title = document.getElementById("title").innerText;
    document.title = title;
    var fc = file_content().innerHTML;
    fc = file_string_trim(fc);
    setCookie("martin",fc);
}

function file_string_trim(string) {
    string = string.trim();
    //string = string.replace(/(\r\n|\n|\r)/gm, "");
    return string;
}


function file_content() {
    var file_content = document.getElementById("main").cloneNode(true); 
    file_content = removeMath(file_content);
    file_content = reappearMath(file_content);
    file_content.innerHTML = encodeMaths(file_content.innerHTML,false)
    hideMakeBoxes(file_content);
    return file_content;

}

function file_export(src) {
    var fc = file_content();
    title = document.title;
    var htmlContent = [fc.innerHTML];
    var bl = new Blob(htmlContent, {type: "text"});
    var a = document.createElement("a");
    a.href = URL.createObjectURL(bl);
    a.download = title+".txt";
    a.hidden = true;
    document.body.appendChild(a);
    a.innerHTML = "something random";
    a.click();
}


function file_new(src) {
    file_export();
    deleteCookie("martin");
    window.location.reload();
}

function file_load(src) {
    var input_field = document.getElementById("load_file");
    input_field.onchange = handle_import;
    input_field.click();
}

function handle_import(e) {
    var f = this.files[0];
    var reader = new FileReader();
    reader.onload = (function(file) {
        setCookie("martin",file.target.result);
        runFile();
    })
    reader.readAsText(f)

}

function getCookie(cname) {
    if (window.localStorage.getItem(cname) != undefined) {
        return window.localStorage.getItem(cname);
    }
    return ""

  }

function deleteCookie(cname) {
    window.localStorage.setItem(cname,"");
    return;    
}

function setCookie(cname, cvalue) {
    window.localStorage.setItem(cname,cvalue);
    return;
}